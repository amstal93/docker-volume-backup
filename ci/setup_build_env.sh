#!/usr/bin/env bash

apt-get update
apt-get install -y --no-install-recommends \
    ca-certificates \
    docker.io \
    make
