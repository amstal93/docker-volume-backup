DOCKERHUB_IMAGE = elritsch/docker-volume-backup

IMAGE ?= $(DOCKERHUB_IMAGE)

.PHONY: all build push

all: build

build:
	docker build --pull --tag ${IMAGE} docker

push:
	docker push ${IMAGE}
